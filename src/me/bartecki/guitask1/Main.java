package me.bartecki.guitask1;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
	// write your code here
    }
}

abstract class Vehicle {
    private Producer producer;
    private LocalDate DateOfManufacture;
    private int Mileage;
    private boolean IsLowFloor;
    private boolean IsArticulated;

    public Vehicle(Producer producer, LocalDate dateOfManufacture, int mileage, boolean isLowFloor, boolean isArticulated) {
        if(producer == null)
        {
            throw new IllegalArgumentException("You have to specify producer of the vehicle");
        }
        this.producer = producer;
        DateOfManufacture = dateOfManufacture;
        Mileage = mileage;
        IsLowFloor = isLowFloor;
        IsArticulated = isArticulated;
    }

    public Producer getProducer() {
        return producer;
    }

    public LocalDate getDateOfManufacture() {
        return DateOfManufacture;
    }

    public boolean isLowFloor() {
        return IsLowFloor;
    }

    public boolean isArticulated() {
        return IsArticulated;
    }

    public int getMileage() {
        return Mileage;
    }

    public void setMileage(int newMileage) {
        if(newMileage < this.Mileage)
        {
            throw new IllegalArgumentException("New mileage has to be higher than previous mileage");
        }
        Mileage = newMileage;
    }

    public int GetAge()
    {
        LocalDate now = LocalDate.now();
        var difference = now.until(DateOfManufacture);
        return difference.getYears();
    }
}

enum FuelType {
    Unknown,
    Diesel,
    Gasoline,
    LiquidPetrolGas
}

class Bus extends Vehicle {
    public BigDecimal getEngineCapacity() {
        return EngineCapacity;
    }

    ///May be possible after engine modernization/exchange
    public void setEngineCapacity(BigDecimal engineCapacity) {
        EngineCapacity = engineCapacity;
    }


    public me.bartecki.guitask1.FuelType getFuelType() {
        return FuelType;
    }

    ///May be possible to for example switch from Gasoline (95) to LPG
    public void setFuelType(me.bartecki.guitask1.FuelType fuelType) {
        if(fuelType.equals(FuelType.Unknown))
        {
            throw new IllegalArgumentException("Fuel type cannot be unknown");
        }
        FuelType = fuelType;
    }

    private BigDecimal EngineCapacity;
    private FuelType FuelType;

    public Bus(Producer producer, LocalDate dateOfManufacture, int mileage, boolean isLowFloor, boolean isArticulated,
               BigDecimal engineCapacity, me.bartecki.guitask1.FuelType fuelType) {
        super(producer, dateOfManufacture, mileage, isLowFloor, isArticulated);
        setEngineCapacity(engineCapacity);
        setFuelType(fuelType);
    }
}

class Tram extends Vehicle {

    public Tram(Producer producer, LocalDate dateOfManufacture, int mileage, boolean isLowFloor, boolean isArticulated) {
        super(producer, dateOfManufacture, mileage, isLowFloor, isArticulated);
    }
}

class Producer {
    public Producer(String name, LocalDate dateEstabilished, String headquarterCity, String website) {
        if(name == null || name.equals(""))
        {
            throw new IllegalArgumentException("Producer name cannot be empty");
        }
        Name = name;
        DateEstabilished = dateEstabilished;
        HeadquarterCity = headquarterCity;
        Website = website;
    }

    private String Name;
    private LocalDate DateEstabilished;
    private String HeadquarterCity;
    private String Website;

    public String getName() {
        return Name;
    }

    public LocalDate getDateEstabilished() {
        return DateEstabilished;
    }

    public String getHeadquarterCity() {
        return HeadquarterCity;
    }

    public String getWebsite() {
        return Website;
    }
}