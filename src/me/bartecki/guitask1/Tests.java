package me.bartecki.guitask1;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class Tests  {
    @Test
    public void Bus_IllegalArgumentException_OnProducerNull() {
        Producer producer = null;
        LocalDate producedAt = LocalDate.parse("1984-10-18");

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            new Bus(producer, producedAt, 1234, false, false, new BigDecimal("3.45"), FuelType.Gasoline);
        });

        String expectedMessage = "You have to specify producer of the vehicle";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void Bus_CanCreate() {
        Producer producer = new Producer("Ford", LocalDate.parse("1840-03-20"), "Las Vegas", "www.ford.com");

        var bus = new Bus(producer, LocalDate.parse("1984-10-18"), 1500, true, false, new BigDecimal(3.20), FuelType.Diesel);

        assertEquals(1500, bus.getMileage());
        assertEquals(producer, bus.getProducer());
    }

    @Test
    public void Bus_IllegalArgumentException_OnMileageReverse() {
        Producer producer = new Producer("Ford", LocalDate.parse("1840-03-20"), "Las Vegas", "www.ford.com");

        var bus = new Bus(producer, LocalDate.parse("1984-10-18"), 1500, true, false, new BigDecimal(3.20), FuelType.Diesel);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            bus.setMileage(1200);
        });
    }

    @Test
    public void Bus_CanSetMileage() {
        Producer producer = new Producer("Ford", LocalDate.parse("1840-03-20"), "Las Vegas", "www.ford.com");

        var bus = new Bus(producer, LocalDate.parse("1984-10-18"), 1500, true, false, new BigDecimal(3.20), FuelType.Diesel);
        bus.setMileage(1600);
        assertEquals(1600, bus.getMileage());
    }
}